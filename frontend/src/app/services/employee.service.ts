import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Employee } from '../models/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  API_URL = 'http://localhost:4000/api/employees';

  constructor(private http: HttpClient) { }

  getEmployees() {
    return this.http.get<Employee[]>(this.API_URL);
  }

  createEmployee(employee: Employee) {
    return this.http.post(this.API_URL, employee);
  }

  deleteEmployee(id: string) {
    return this.http.delete(`${this.API_URL}/${id}`)
  }

  updateEmployee(employee: Employee) {
    return this.http.put(`${this.API_URL}/${employee._id}`, employee);
  }
}
